#include <linux/slab.h>
#include "netint_fence.h"

static struct kmem_cache *ni_fence_cache;
atomic64_t ni_timeline_count;
atomic64_t ni_fence_count;

struct ni_fence *ni_fence_alloc(void)
{
    struct ni_fence *ni_fence;

    ni_fence = kmem_cache_alloc(ni_fence_cache, GFP_KERNEL);
    if (ni_fence) {
        INIT_LIST_HEAD(&ni_fence->list);
        ni_fence->timeline = NULL;
        atomic64_inc(&ni_fence_count);
    }
    return ni_fence;
}

void ni_fence_free(struct ni_fence *ni_fence)
{
    if (ni_fence) {
        kmem_cache_free(ni_fence_cache, ni_fence);
        atomic64_dec(&ni_fence_count);
    }
}

void ni_timeline_put(struct ni_timeline *timeline)
{
    if (atomic_dec_and_test(&timeline->refcount)) {
        atomic64_dec(&ni_timeline_count);
        kfree(timeline);
    }
}

struct ni_timeline *ni_timeline_alloc(void)
{
    struct ni_timeline *timeline;

    timeline = kzalloc(sizeof(struct ni_timeline), GFP_KERNEL);
    if (!timeline)
        return NULL;

    spin_lock_init(&timeline->lock);
    mutex_init(&timeline->mutex);
    atomic_set(&timeline->refcount, 1);
    timeline->context = dma_fence_context_alloc(1);
    timeline->seqno = timeline->hwseqno = 0;

    atomic64_inc(&ni_timeline_count);

    return timeline;
}

void ni_fence_put(struct ni_fence *ni_fence)
{
    if (ni_fence)
        dma_fence_put(&ni_fence->fence);
}

void ni_fence_init_context(struct ni_fence *ni_fence,
        const struct dma_fence_ops *ops,
        struct ni_timeline *timeline)
{
    atomic_inc(&timeline->refcount);

    ni_fence->timeline = timeline;
    dma_fence_init(&ni_fence->fence, ops, &timeline->lock,
            (u64)timeline->context, ++timeline->seqno);
}

int ni_fence_init(void)
{
    ni_fence_cache = kmem_cache_create("ni_fence", sizeof(struct ni_fence), 0,
            SLAB_HWCACHE_ALIGN, NULL);
    if (!ni_fence_cache) {
        ni_err("failed to create fence cache\n");
        return -ENOMEM;
    }

    atomic64_set(&ni_fence_count, 0);
    atomic64_set(&ni_timeline_count, 0);

    return 0;
}

void ni_fence_cleanup(void)
{
    if (ni_fence_cache) {
        if (atomic64_read(&ni_fence_count) != 0)
            ni_warn("ni_fence count %ld isn't zero!!!\n",
                    (long)atomic64_read(&ni_fence_count));
        kmem_cache_destroy(ni_fence_cache);
        ni_fence_cache = NULL;
    }

    if (atomic64_read(&ni_timeline_count) != 0)
        ni_warn("timeline count %ld isn't zero!!!\n",
                (long)atomic64_read(&ni_timeline_count));
}
