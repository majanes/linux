#ifndef _NETINT_FENCE_H
#define _NETINT_FENCE_H

#include "netint.h"

extern void ni_fence_init_context(struct ni_fence *ni_fence,
        const struct dma_fence_ops *ops,
        struct ni_timeline *timeline);

extern struct ni_timeline *ni_timeline_alloc(void);
extern void ni_timeline_put(struct ni_timeline *ni_timeline);

extern struct ni_fence *ni_fence_alloc(void);
extern void ni_fence_free(struct ni_fence *ni_fence);
extern void ni_fence_put(struct ni_fence *ni_fence);
extern int ni_fence_init(void);
extern void ni_fence_cleanup(void);
#endif
