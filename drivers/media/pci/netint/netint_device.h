#ifndef _NETINT_DEVICE_H
#define _NETINT_DEVICE_H

#include "netint.h"

extern struct ni_dmabuf_desc *ni_dmabuf_desc_alloc(struct pci_dev *pci_dev,
        int bar, unsigned long offset, unsigned long length);
extern void ni_dmabuf_desc_put(struct ni_dmabuf_desc *dbuf_desc);
extern int ni_dmabuf_desc_slab_init(void);
extern void ni_dmabuf_desc_slab_exit(void);
#endif
