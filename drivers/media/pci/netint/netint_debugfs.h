#ifndef _NETINT_DEBUGFS_H
#define _NETINT_DEBUGFS_H

#include "netint.h"
#include <linux/debugfs.h>

extern int ni_debugfs_init(void);
extern void ni_debugfs_exit(void);
#endif
