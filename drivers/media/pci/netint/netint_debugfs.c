#include <linux/debugfs.h>
#include "netint.h"
#include "netint_debugfs.h"

static struct dentry *ni_debugfs_root;

extern atomic64_t ni_timeline_count;
extern atomic64_t ni_fence_count;
#ifdef NETINT_VERIFY_DMABUF
extern atomic64_t ni_ioreq_count;
#endif
extern atomic64_t ni_dmabuf_desc_count;

static struct dentry *ni_debugfs_stats;

static int ni_stats_debugfs_seq_show(struct seq_file *s, void *v)
{
    seq_printf(s, "timeline_count %ld\n", (long)atomic64_read(&ni_timeline_count));
    seq_printf(s, "fence_count %ld\n", (long)atomic64_read(&ni_fence_count));
#ifdef NETINT_VERIFY_DMABUF
    seq_printf(s, "ioreq_count %ld\n", (long)atomic64_read(&ni_ioreq_count));
#endif
    seq_printf(s, "dmabuf_desc_count %ld\n", (long)atomic64_read(&ni_dmabuf_desc_count));

    return 0;
}

static int ni_stats_debugfs_open(struct inode *inode, struct file *file)
{
    return single_open(file, ni_stats_debugfs_seq_show, inode->i_private);
}

static struct file_operations debugfs_stats_fops = {
    .owner = THIS_MODULE,
    .open = ni_stats_debugfs_open,
    .read = seq_read,
    .llseek = seq_lseek,
    .release = single_release,
};

int ni_debugfs_init(void)
{
    ni_debugfs_root = debugfs_create_dir("netint", NULL);
    if (!ni_debugfs_root) {
        ni_err("failed to create debugfs root\n");
        return -ENOMEM;
    }

    ni_debugfs_stats = debugfs_create_file("stats", 0444, ni_debugfs_root, NULL,
            &debugfs_stats_fops);
    if (!ni_debugfs_stats) {
        ni_err("failed to create stats file\n");
        goto destroy_debugfs_root;
    }

    return 0;
destroy_debugfs_root:
    debugfs_remove_recursive(ni_debugfs_root);
    return -ENOMEM;
}

void ni_debugfs_exit(void)
{
    if (ni_debugfs_root) {
        if (ni_debugfs_stats) {
            debugfs_remove(ni_debugfs_stats);
            ni_debugfs_stats = NULL;
        }

        debugfs_remove_recursive(ni_debugfs_root);
        ni_debugfs_root = NULL;
    }
}
