#include <linux/mm.h>
#include <linux/version.h>
#include <linux/dma-fence-array.h>

#include "netint.h"
#include "netint_export.h"
#include "netint_import.h"
#include "netint_device.h"
#include "netint_fence.h"

#define NI_BAD_DMA_ADDRESS (~(dma_addr_t)0)

static int write_combine_allow = 0;

extern void ni_dev_put(struct ni_dev *dev);
extern void ni_dmabuf_desc_put(struct ni_dmabuf_desc *dbuf_desc);

#ifdef RHEL_RELEASE_CODE
#if RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7, 7)
static int ni_dmabuf_ops_attach(struct dma_buf *dmabuf,
        struct dma_buf_attachment *dmabuf_attach)
#else
static int ni_dmabuf_ops_attach(struct dma_buf *dmabuf,
        struct device *device,
        struct dma_buf_attachment *dmabuf_attach)
#endif
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(4, 19, 0))
static int ni_dmabuf_ops_attach(struct dma_buf *dmabuf,
        struct dma_buf_attachment *dmabuf_attach)
#else
static int ni_dmabuf_ops_attach(struct dma_buf *dmabuf,
        struct device *device,
        struct dma_buf_attachment *dmabuf_attach)
#endif
{
    struct ni_dmabuf_attachment *attach;
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    attach = kzalloc(sizeof(*attach), GFP_KERNEL);
    if (!attach)
        return -ENOMEM;

    attach->dma_handle = NI_BAD_DMA_ADDRESS;
    attach->dev = dmabuf_attach->dev;
    /* get a copy */
    dbuf_desc->attach = dmabuf_attach->priv = attach;
    return 0;
}

static void ni_dmabuf_ops_detach(struct dma_buf *dmabuf,
        struct dma_buf_attachment *dmabuf_attach)
{
    struct ni_dmabuf_attachment *attach;
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    attach = dmabuf_attach->priv;
    if (!attach)
        return;

    kfree(attach);
    dbuf_desc->attach = dmabuf_attach->priv = NULL;
}

static struct sg_table *ni_dmabuf_ops_map(
        struct dma_buf_attachment *dmabuf_attach,
        enum dma_data_direction dma_dir)
{
    struct dma_buf *dmabuf = dmabuf_attach->dmabuf;
    struct ni_dmabuf_attachment *attach = dmabuf_attach->priv;
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;
    struct sg_table *sgtable;
    int ret;
    dma_addr_t dma_handle;

    sgtable = kzalloc(sizeof(struct sg_table), GFP_KERNEL);
    if (!sgtable) {
        ni_err("failed to allocate sgtable\n");
        return ERR_PTR(-ENOMEM);
    }

    ret = sg_alloc_table(sgtable, 1, GFP_KERNEL);
    if (ret < 0) {
        ni_err("failed to get sglist\n");
        goto free_sgt;
    }

    dma_handle = dma_map_resource(dmabuf_attach->dev,
            dbuf_desc->phys_addr, dbuf_desc->len,
            dma_dir, 0);
    if (dma_mapping_error(dmabuf_attach->dev, dma_handle)) {
        ni_err("failed to map dma. phys_addr 0x%lx, len 0x%x\n",
                dbuf_desc->phys_addr, dbuf_desc->len);
        ret = -ENOMEM;
        goto free_sgt_table;
    }

    sg_set_page(sgtable->sgl, NULL, dbuf_desc->len, 0);
    sg_dma_address(sgtable->sgl) = attach->dma_handle =
            dma_handle;
    sg_dma_len(sgtable->sgl) = dbuf_desc->len;
    return sgtable;

free_sgt_table:
    sg_free_table(sgtable);
free_sgt:
    kfree(sgtable);
    return ERR_PTR(ret);
}

static void ni_dmabuf_ops_unmap(struct dma_buf_attachment *dmabuf_attach,
        struct sg_table *sgtable, enum dma_data_direction dma_dir)
{
    struct dma_buf *dmabuf = dmabuf_attach->dmabuf;
    struct ni_dmabuf_attachment *attach = dmabuf_attach->priv;
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    dma_unmap_resource(dmabuf_attach->dev,
            sg_dma_address(sgtable->sgl),
            dbuf_desc->len, dma_dir, 0);
    sg_free_table(sgtable);
    kfree(sgtable);
    attach->dma_handle = NI_BAD_DMA_ADDRESS;
}

#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 5, 0)
static void *ni_dmabuf_ops_kmap(struct dma_buf *dmabuf, unsigned long pgnum)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    return dbuf_desc->vaddr ? dbuf_desc->vaddr + pgnum * PAGE_SIZE : NULL;
}
#endif

#if defined(RHEL_RELEASE_CODE)
#if RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7, 6)
static void *ni_dmabuf_ops_kmap_atomic(struct dma_buf *dma_buf, unsigned long page_num)
{
    return NULL;
}
#endif
#else
#if LINUX_VERSION_CODE <= KERNEL_VERSION(4, 19, 0)
static void *ni_dmabuf_ops_kmap_atomic(struct dma_buf *dma_buf, unsigned long page_num)
{
    return NULL;
}
#endif
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 18, 0)
static int ni_dmabuf_ops_vmap(struct dma_buf *dmabuf, struct iosys_map *map)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    iosys_map_set_vaddr(map, dbuf_desc->vaddr);
    return 0;
}
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 11, 0)
static int ni_dmabuf_ops_vmap(struct dma_buf *dmabuf, struct dma_buf_map *map)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    dma_buf_map_set_vaddr(map, dbuf_desc->vaddr);
    return 0;
}
#else
static void *ni_dmabuf_ops_vmap(struct dma_buf *dmabuf)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    return dbuf_desc->vaddr;
}
#endif

static const struct vm_operations_struct dma_pci_phys_vm_ops = {
#ifdef CONFIG_HAVE_IOREMAP_PROT
    .access = generic_access_phys,
#endif
};

// remap to userspace. refer to drivers/pci/pci-sysfs.c
static int ni_dmabuf_ops_mmap(struct dma_buf *dmabuf, struct vm_area_struct *vma)
{
//    int ret;
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    /* writecombine */
    if (write_combine_allow &&
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
            arch_can_pci_mmap_wc() &&
#endif
            dbuf_desc->flags & IORESOURCE_PREFETCH)
        vma->vm_page_prot = pgprot_writecombine(vma->vm_page_prot);
    else
        vma->vm_page_prot = pgprot_device(vma->vm_page_prot);

    if (vma->vm_pgoff + vma_pages(vma) > dbuf_desc->len)
        return -EINVAL;

    vma->vm_pgoff += dbuf_desc->phys_addr >> PAGE_SHIFT;
//    vma->vm_private_data = dbus_desc;
    vma->vm_ops = &dma_pci_phys_vm_ops;

    ni_debug("vm_pgoff %lu, vm_page_prot 0x%lx\n", vma->vm_pgoff,
            vma->vm_page_prot.pgprot);

    return io_remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
            vma->vm_end - vma->vm_start,
            vma->vm_page_prot);
}

static void ni_dmabuf_ops_release(struct dma_buf *dmabuf)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;
    struct dma_buf_attachment *dmabuf_attach;
    struct ni_dmabuf_attachment *ni_dbuf_attach;

    ni_dbuf_attach = dbuf_desc->attach;
    if (ni_dbuf_attach) {
        if (ni_dbuf_attach->src_dev) {
            pci_dev_put(ni_dbuf_attach->src_dev);
            ni_dbuf_attach->src_dev = NULL;
        }
    }

    dmabuf_attach = dbuf_desc->db_attach;
    if (dmabuf_attach) {
        if (dbuf_desc->sgtable) {
            dma_buf_unmap_attachment(dmabuf_attach,
                dbuf_desc->sgtable, dbuf_desc->dma_dir);
            dbuf_desc->sgtable = NULL;
        }
        dma_buf_detach(dbuf_desc->db_attach->dmabuf, dbuf_desc->db_attach);

    }

    ni_dmabuf_desc_put(dbuf_desc);
}

static int ni_dmabuf_ops_begin_cpu_access(struct dma_buf *dmabuf,
        enum dma_data_direction direction)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    if (!dbuf_desc->attach)
        return -ENODEV;

    if (dbuf_desc->attach->dma_handle == NI_BAD_DMA_ADDRESS)
        return -ENOMEM;

    dma_sync_single_for_cpu(dbuf_desc->attach->dev,
            dbuf_desc->attach->dma_handle, dbuf_desc->len,
            direction);
    return 0;
}

static int ni_dmabuf_ops_end_cpu_access(struct dma_buf *dmabuf,
        enum dma_data_direction direction)
{
    struct ni_dmabuf_desc *dbuf_desc = dmabuf->priv;

    if (!dbuf_desc->attach)
        return -ENODEV;

    if (dbuf_desc->attach->dma_handle == NI_BAD_DMA_ADDRESS)
        return -ENOMEM;

    dma_sync_single_for_device(dbuf_desc->attach->dev,
            dbuf_desc->attach->dma_handle, dbuf_desc->len,
            direction);
    return 0;
}

static const struct dma_buf_ops ni_dmabuf_ops = {
    .attach = ni_dmabuf_ops_attach,
    .detach = ni_dmabuf_ops_detach,
    .map_dma_buf = ni_dmabuf_ops_map,
    .unmap_dma_buf = ni_dmabuf_ops_unmap,
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 5, 0)
    .map = ni_dmabuf_ops_kmap,
#endif
#if defined(RHEL_RELEASE_CODE)
#if RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7, 6)
    .map_atomic = ni_dmabuf_ops_kmap_atomic,
#endif
#else
#if LINUX_VERSION_CODE <= KERNEL_VERSION(4, 19, 0)
    .map_atomic = ni_dmabuf_ops_kmap_atomic,
#endif
#endif
    .vmap = ni_dmabuf_ops_vmap,
    .mmap = ni_dmabuf_ops_mmap,
    .release = ni_dmabuf_ops_release,
    .begin_cpu_access = ni_dmabuf_ops_begin_cpu_access,
    .end_cpu_access = ni_dmabuf_ops_end_cpu_access,
};

struct dma_buf *ni_dmabuf_export(struct ni_dmabuf_desc *dbuf_desc)
{
    struct dma_buf *dmabuf;
    DEFINE_DMA_BUF_EXPORT_INFO(exp_info);
    int ret;

    exp_info.flags = O_RDWR;
    exp_info.ops = &ni_dmabuf_ops;
    exp_info.size = dbuf_desc->len;
    exp_info.priv = dbuf_desc;

    dmabuf = dma_buf_export(&exp_info);
    if (IS_ERR(dmabuf))
        ni_err("failed to export dmabuf\n");
    else {
        BUG_ON(!dmabuf->resv);
#if defined(RHEL_RELEASE_CODE)
#if RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7, 7)
        ret = reservation_object_reserve_shared(dmabuf->resv, 1);
#else
        ret = reservation_object_reserve_shared(dmabuf->resv);
#endif
#else
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
        ret = dma_resv_reserve_fences(dmabuf->resv, 1);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
        ret = dma_resv_reserve_shared(dmabuf->resv, 1);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 0, 0)
        ret = reservation_object_reserve_shared(dmabuf->resv, 1);
#else
        ret = reservation_object_reserve_shared(dmabuf->resv);
#endif
#endif
        if (ret < 0) {
            ni_err("failed to reserve shared objects\n");
            dma_buf_put(dmabuf);
            dmabuf = ERR_PTR(ret);
        }
    }

    return dmabuf;
}

static const char *ni_dma_fence_get_driver_name(struct dma_fence *fence)
{
    return "ni_dev";
}

static const char *ni_dma_fence_get_timeline_name(struct dma_fence *fence)
{
    if (test_bit(DMA_FENCE_FLAG_SIGNALED_BIT, &fence->flags))
        return "signaled";

    return "ni_dma_fence";
}

static bool ni_dma_fence_enable_signaling(struct dma_fence *fence)
{
    return true;
}

static inline bool ni_fence_passed(u64 seq1, u64 seq2)
{
    return (s64)(seq1 - seq2) >= 0;
}

static bool ni_dma_fence_signaled(struct dma_fence *fence)
{
//    struct ni_fence *ni_fence = container_of(fence, struct ni_fence, fence);

    if (test_bit(DMA_FENCE_FLAG_SIGNALED_BIT, &fence->flags))
        return true;

//    if (ni_fence_passed(ni_fence->timeline->hwseqno, ni_fence->fence.seqno))
//        return true;

    return false;
}

static void ni_dma_fence_release(struct dma_fence *fence)
{
    struct ni_fence *ni_fence = container_of(fence, struct ni_fence, fence);

    if (ni_fence->timeline)
        ni_timeline_put(ni_fence->timeline);
    ni_fence_free(ni_fence);
}

static const struct dma_fence_ops ni_dma_fence_ops = {
    .get_driver_name = ni_dma_fence_get_driver_name,
    .get_timeline_name = ni_dma_fence_get_timeline_name,
    .enable_signaling = ni_dma_fence_enable_signaling,
    .signaled = ni_dma_fence_signaled,
    .wait = dma_fence_default_wait,
    .release = ni_dma_fence_release,
};

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
static int ni_dmabuf_sync_read(struct ni_dev *ni_dev, struct dma_buf *dmabuf)
{
    struct dma_resv_iter cursor;
    struct dma_fence *f;
    int ret = 0;

    dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_WRITE);
    dma_resv_for_each_fence_unlocked(&cursor, f) {
        ni_debug("%s: wait for fence usage %d\n", __func__, cursor.usage);
        ret = dma_fence_wait(f, true);
        if (ret < 0) {
            ni_err("%s: failed to wait fence: usage %d\n", __func__,
                    cursor.usage);
            dma_resv_iter_end(&cursor);
            return ret;
        }
    }
    dma_resv_iter_end(&cursor);
    return ret;
}

static int ni_dmabuf_sync_write(struct ni_dev *ni_dev, struct dma_buf *dmabuf)
{
    struct dma_resv_iter cursor;
    struct dma_fence *f;
    int ret = 0;

    dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_READ);
    dma_resv_for_each_fence_unlocked(&cursor, f) {
        /* strictly wait for every write fence and read fence */
        ret = dma_fence_wait(f, true);
        if (ret < 0) {
            ni_err("%s: failed to wait fence: usage %d\n", __func__,
                    cursor.usage);
            dma_resv_iter_end(&cursor);
            return ret;
        }
    }
    dma_resv_iter_end(&cursor);
    return ret;
}
#else
static int ni_dmabuf_sync_read(struct ni_dev *ni_dev, struct dma_buf *dmabuf)
{
    struct dma_fence *excl = NULL;
    int ret = 0;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 14, 0)
    excl = dma_resv_get_excl_unlocked(dmabuf->resv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    excl = dma_resv_get_excl_rcu(dmabuf->resv);
#else
    excl = reservation_object_get_excl_rcu(dmabuf->resv);
#endif
    if (excl) {
        ni_debug("%s: syncup external write fence\n", __func__);
        ret = dma_fence_wait(excl, true);
        if (ret < 0)
            ni_err("failed to wait for write fence\n");

        dma_fence_put(excl);
    }

    return ret;
}

static int ni_dmabuf_sync_write(struct ni_dev *ni_dev, struct dma_buf *dmabuf)
{
    struct dma_fence *excl = NULL;
    struct dma_fence **shared = NULL;
    int count;
    int ret = 0;
    int i = 0;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 14, 0)
    ret = dma_resv_get_fences(dmabuf->resv,
            &excl, &count, &shared);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    ret = dma_resv_get_fences_rcu(dmabuf->resv,
            &excl, &count, &shared);
#else
    ret = reservation_object_get_fences_rcu(dmabuf->resv,
            &excl, &count, &shared);
#endif
    if (ret < 0) {
        ni_err("%s: failed to get fence\n", __func__);
        return ret;
    }

    ni_debug("%s: shared fence count %d\n", __func__, count);
    for (i = 0; i < count; i++) {
        ret = dma_fence_wait(shared[i], true);
        dma_fence_put(shared[i]);
        if (ret < 0) {
            ni_err("%s: failed to wait read fence, id %d\n", __func__, i);
            goto out;
        }
    }

    if (excl) {
        if (excl->ops != &ni_dma_fence_ops ||
                false == dma_fence_match_context(excl,
                    ni_dev->timeline->context)) {
            ret = dma_fence_wait(excl, true);
            if (ret < 0)
                ni_err("failed to wait write fence\n");
        }
    }
out:
    if (shared) {
        for (i = i + 1; i < count; i++)
            dma_fence_put(shared[i]);
        kfree(shared);
    }
    if (excl)
        dma_fence_put(excl);
    return ret;
}
#endif

static int ni_dmabuf_sync_external_fence(struct ni_dev *ni_dev,
        struct dma_buf *dmabuf, int exclusive)
{
    int ret = 0;

    if (!dmabuf->resv)
        return 0;

    if (exclusive) {
        ret = ni_dmabuf_sync_write(ni_dev, dmabuf);
    } else {
        ret = ni_dmabuf_sync_read(ni_dev, dmabuf);
    }
    return ret;
}

static inline struct dma_buf *ni_fence_get_dmabuf(int fd)
{

    struct dma_buf *dmabuf = dma_buf_get(fd);
    if (IS_ERR(dmabuf))
        return dmabuf;

    if (dmabuf->ops != &ni_dmabuf_ops) {
        dma_buf_put(dmabuf);
        return ERR_PTR(-EINVAL);
    }

    return dmabuf;
}

int ni_dmabuf_attach_fence(struct ni_dev *ni_dev, int fd,
        struct ni_fence *ni_fence, int exclusive)
{
    struct dma_buf *dmabuf;
    int ret;

    dmabuf = ni_fence_get_dmabuf(fd);
    if (IS_ERR(dmabuf)) {
        ni_err("%s: failed to get dmabuf from fd %d\n",
                __func__, fd);
        return PTR_ERR(dmabuf);
    }

    ret = ni_dmabuf_sync_external_fence(ni_dev, dmabuf, exclusive);
    if (ret < 0) {
        ni_err("failed to sync up external fence, %d\n", fd);
        goto put_dmabuf;
    }

    mutex_lock(&ni_dev->timeline->mutex);
    ni_fence_init_context(ni_fence, &ni_dma_fence_ops,
            ni_dev->timeline);
    mutex_unlock(&ni_dev->timeline->mutex);

    spin_lock(&ni_dev->fence_lock);
    list_add_tail(&ni_fence->list, &ni_dev->fences);
    spin_unlock(&ni_dev->fence_lock);

    BUG_ON(!dmabuf->resv);
    if (exclusive) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
        dma_resv_lock(dmabuf->resv, NULL);
        ret = dma_resv_reserve_fences(dmabuf->resv, 1);
        if (ret) {
            ni_err("failed to reserve fence slot for write: fd %d\n", fd);
            dma_resv_unlock(dmabuf->resv);
            goto put_dmabuf;
        }
        dma_resv_add_fence(dmabuf->resv, &ni_fence->fence, DMA_RESV_USAGE_WRITE);
        dma_resv_unlock(dmabuf->resv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
        dma_resv_lock(dmabuf->resv, NULL);
        dma_resv_add_excl_fence(dmabuf->resv, &ni_fence->fence);
        dma_resv_unlock(dmabuf->resv);
#else
        reservation_object_lock(dmabuf->resv, NULL);
        reservation_object_add_excl_fence(dmabuf->resv, &ni_fence->fence);
        reservation_object_unlock(dmabuf->resv);
#endif
    } else {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
        dma_resv_lock(dmabuf->resv, NULL);
        ret = dma_resv_reserve_fences(dmabuf->resv, 1);
        if (ret) {
            ni_err("failed to reserve fence slot for read: fd %d\n", fd);
            dma_resv_unlock(dmabuf->resv);
            goto put_dmabuf;
        }
        dma_resv_add_fence(dmabuf->resv, &ni_fence->fence, DMA_RESV_USAGE_READ);
        dma_resv_unlock(dmabuf->resv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
        dma_resv_lock(dmabuf->resv, NULL);
        dma_resv_add_shared_fence(dmabuf->resv, &ni_fence->fence);
        dma_resv_unlock(dmabuf->resv);
#else
        reservation_object_lock(dmabuf->resv, NULL);
        reservation_object_add_shared_fence(dmabuf->resv, &ni_fence->fence);
        reservation_object_unlock(dmabuf->resv);
#endif
    }
put_dmabuf:
    dma_buf_put(dmabuf);
    return ret;
}

void ni_free_fence(struct ni_dev *ni_dev, struct dma_fence *fence)
{
    struct ni_fence *ni_fence = container_of(fence, struct ni_fence, fence);

    if (ni_fence->timeline != ni_dev->timeline) {
        BUG_ON(0);
        return;
    }

    spin_lock(&ni_dev->fence_lock);
    list_del(&ni_fence->list);
    spin_unlock(&ni_dev->fence_lock);

    ni_fence_put(ni_fence);
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
static int ni_dmabuf_release_exclusive(struct ni_dev *ni_dev,
        struct ni_timeline *timeline, struct dma_buf *dmabuf, int fd)
{
    struct dma_resv_iter cursor;
    struct dma_fence *f;
    int ret = 0;

    mutex_lock(&timeline->mutex);
    timeline->hwseqno++;
    ni_debug("%s: excl fence, wtimeline hwseqno %llu, fd %d\n",
            __func__, timeline->hwseqno, fd);
    dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_WRITE);
    dma_resv_for_each_fence_unlocked(&cursor, f) {
        if (f->ops != &ni_dma_fence_ops ||
                false == dma_fence_match_context(f,
                    timeline->context)) {
            ni_debug("%s: not ours fence, fd %d\n", __func__, fd);
        } else {
            ni_debug("%s: signal write fence, fd %d\n", __func__, fd);
            dma_fence_signal(f);
            ni_free_fence(ni_dev, f);
        }
    }
    dma_resv_iter_end(&cursor);
    mutex_unlock(&timeline->mutex);
    return ret;
}

static int ni_dmabuf_release_share(struct ni_dev *ni_dev,
        struct ni_timeline *timeline, struct dma_buf *dmabuf, int fd)
{
    struct dma_resv_iter cursor;
    struct dma_fence *f;
    int ret = 0;

    mutex_lock(&timeline->mutex);
    ni_dev->timeline->hwseqno++;
    ni_debug("%s: shared fence rtimeline hwseqno %llu, fd %d\n",
            __func__, ni_dev->timeline->hwseqno, fd);
    dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_READ);
    dma_resv_for_each_fence_unlocked(&cursor, f) {
        if (f->ops != &ni_dma_fence_ops ||
                false == dma_fence_match_context(f,
                    ni_dev->timeline->context)) {
            ni_debug("%s: not ours fence fd %d\n", __func__, fd);
            continue;
        }

        ni_debug("%s: signal read fence fd %d\n", __func__, fd);
        dma_fence_signal(f);
        ni_free_fence(ni_dev, f);
    }
    dma_resv_iter_end(&cursor);
    mutex_unlock(&timeline->mutex);
    return ret;
}
#else
static int ni_dmabuf_release_exclusive(struct ni_dev *ni_dev,
        struct ni_timeline *timeline, struct dma_buf *dmabuf, int fd)
{
    struct dma_fence *excl = NULL;
    int ret = 0;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 14, 0)
    excl = dma_resv_get_excl_unlocked(dmabuf->resv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    excl = dma_resv_get_excl_rcu(dmabuf->resv);
#else
    excl = reservation_object_get_excl_rcu(dmabuf->resv);
#endif

    mutex_lock(&timeline->mutex);
    timeline->hwseqno++;
    ni_debug("%s: excl fence, wtimeline hwseqno %llu, fd %d\n",
            __func__, timeline->hwseqno, fd);
    if (excl) {
        if (excl->ops != &ni_dma_fence_ops ||
                false == dma_fence_match_context(excl,
                    timeline->context)) {
            ni_debug("%s: not ours fence, fd %d\n", __func__, fd);
        } else {
            ni_debug("%s: signal write fence, fd %d\n", __func__, fd);
            dma_fence_signal(excl);
            ni_free_fence(ni_dev, excl);
        }
        dma_fence_put(excl);
    }
    mutex_unlock(&timeline->mutex);
    return ret;
}

static int ni_dmabuf_release_share(struct ni_dev *ni_dev,
        struct ni_timeline *timeline, struct dma_buf *dmabuf, int fd)
{
    struct dma_fence **shared = NULL;
    int count;
    int ret = 0;
    int i;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 14, 0)
    ret = dma_resv_get_fences(dmabuf->resv, NULL, &count, &shared);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    ret = dma_resv_get_fences_rcu(dmabuf->resv, NULL, &count, &shared);
#else
    ret = reservation_object_get_fences_rcu(dmabuf->resv, NULL, &count, &shared);
#endif
    if (ret) {
        ni_err("failed to get shared objects, fd %d\n", fd);
        return ret;
    }

    mutex_lock(&timeline->mutex);
    ni_dev->timeline->hwseqno++;
    ni_debug("%s: shared fence count %d, rtimeline hwseqno %llu, fd %d\n",
            __func__, count, ni_dev->timeline->hwseqno, fd);
    for (i = 0; i < count; i++) {
        if (shared[i]->ops != &ni_dma_fence_ops ||
                false == dma_fence_match_context(shared[i],
                    ni_dev->timeline->context)) {
            ni_debug("%s: not ours fence %d, fd %d\n", __func__, i, fd);
            dma_fence_put(shared[i]);
            continue;
        }

        ni_debug("%s: signal read fence %d, fd %d\n", __func__, i, fd);
        dma_fence_signal(shared[i]);
        ni_free_fence(ni_dev, shared[i]);
        dma_fence_put(shared[i]);
    }
    mutex_unlock(&timeline->mutex);
    kfree(shared);
    return ret;
}
#endif

int ni_dmabuf_signal_fence(struct ni_dev *ni_dev, int fd, int exclusive)
{
    struct dma_buf *dmabuf;
    struct ni_timeline *timeline = ni_dev->timeline;
    int ret;

    dmabuf = ni_fence_get_dmabuf(fd);
    if (IS_ERR(dmabuf)) {
        ni_err("%s: failed to get dmabuf from fd %d\n",
                __func__, fd);
        return PTR_ERR(dmabuf);
    }

    BUG_ON(!dmabuf->resv);
    if (exclusive) {
        ret = ni_dmabuf_release_exclusive(ni_dev, timeline, dmabuf, fd);
    } else {
        ret = ni_dmabuf_release_share(ni_dev, timeline, dmabuf, fd);
    }

    dma_buf_put(dmabuf);
    return ret;
}
