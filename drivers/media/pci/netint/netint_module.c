#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/dma-buf.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/uio.h>
#include <linux/uaccess.h>
#include <linux/highmem.h>
#include <linux/sync_file.h>
#include <linux/idr.h>

#include <linux/dma-fence-array.h>
#include "netint.h"
#include "netint_import.h"
#include "netint_export.h"
#include "netint_ioreq.h"
#include "netint_device.h"
#include "netint_ioctl.h"
#include "netint_fence.h"
#include "netint_debugfs.h"

static int netint_major;
static struct cdev ni_cdev;

static struct class *netint_class;
static struct device *netint_device;

static struct ida ni_dev_ida;

int ni_debug_level = 0;
module_param(ni_debug_level, int, S_IRUGO|S_IWUSR);

static int ni_open(struct inode *inode, struct file *file)
{
    struct ni_dev *dev;
    int ret;

    dev = kzalloc(sizeof(struct ni_dev), GFP_KERNEL);
    if (!dev)
        return -ENOMEM;

    ret = ida_simple_get(&ni_dev_ida, 0, 0, GFP_KERNEL);
    if (ret < 0)
        goto free_dev;

    dev->devid = ret;
    ni_debug("create ni_dev id %d\n", dev->devid);

    ret = ni_dev_init_ioreq(dev);
    if (ret < 0)
        goto free_ida;

    mutex_init(&dev->mutex);

    dev->timeline = ni_timeline_alloc();
    if (!dev->timeline) {
        ret = -ENOMEM;
        goto free_ioreq;
    }

    /* borrow ni dev to keep one refcount of each fence. */
    INIT_LIST_HEAD(&dev->fences);
    spin_lock_init(&dev->fence_lock);
    file->private_data = (void *)dev;
    memset(dev->dma_bufs, 0, sizeof(dev->dma_bufs));
    for(unsigned int dma_buf_index = 0; dma_buf_index < MAX_DMA_BUF_PER_DEV; dma_buf_index++) {   
        dev->dma_bufs[dma_buf_index].fd = -1;
    }
    return 0;

free_ioreq:
    ni_dev_free_ioreq(dev);
free_ida:
    ida_simple_remove(&ni_dev_ida, dev->devid);
free_dev:
    kfree(dev);
    return ret;
}

static void ni_dev_put_fence(struct ni_dev *ni_dev)
{
    int count = 0;

    spin_lock(&ni_dev->fence_lock);
    while (!list_empty(&ni_dev->fences)) {
        struct ni_fence *ni_fence = list_first_entry(&ni_dev->fences,
                struct ni_fence, list);
        list_del(&ni_fence->list);
        ni_fence_put(ni_fence);
        if (count++ == 1024) {
            count = 0;
            spin_unlock(&ni_dev->fence_lock);
            cond_resched();
            spin_lock(&ni_dev->fence_lock);
        }
    }
    spin_unlock(&ni_dev->fence_lock);
}

static int ni_release(struct inode *inode, struct file *file)
{
    struct ni_dev *dev = (struct ni_dev *)file->private_data;
    if (dev) {
        ni_debug("destroy ni_dev %d\n", dev->devid);
        ni_dev_put_fence(dev);

        ni_timeline_put(dev->timeline);
        ni_dev_free_ioreq(dev);
        ida_simple_remove(&ni_dev_ida, dev->devid);

        for(unsigned int dma_buf_index = 0; dma_buf_index < MAX_DMA_BUF_PER_DEV; dma_buf_index++) {   
            if (dev->dma_bufs[dma_buf_index].fd != -1) {
                if (dev->dma_bufs[dma_buf_index].pci_dev) {
                    pci_dev_put(dev->dma_bufs[dma_buf_index].pci_dev);
                }

                if (dev->dma_bufs[dma_buf_index].dmabuf_attach && dev->dma_bufs[dma_buf_index].sgtable) {
                    dma_buf_unmap_attachment(dev->dma_bufs[dma_buf_index].dmabuf_attach, dev->dma_bufs[dma_buf_index].sgtable, DMA_BIDIRECTIONAL);
                    dma_buf_detach(dev->dma_bufs[dma_buf_index].dmabuf, dev->dma_bufs[dma_buf_index].dmabuf_attach);
                }
                /* Cleanup any imported dma-buf */
                dma_buf_put(dev->dma_bufs[dma_buf_index].dmabuf);
                dev->dma_bufs[dma_buf_index].pci_dev = NULL;
                dev->dma_bufs[dma_buf_index].sgtable = NULL;
                dev->dma_bufs[dma_buf_index].dmabuf_attach = NULL;
                dev->dma_bufs[dma_buf_index].dmabuf = NULL;
                dev->dma_bufs[dma_buf_index].fd = -1;
            }
        }

        kfree(dev);
        file->private_data = NULL;
    }
    return 0;
}

static int ni_dev_dma_export_dmabuf(struct ni_dev *dev,
        struct netint_iocmd_export_dmabuf __user *uexp)
{
    struct netint_iocmd_export_dmabuf exp;
    struct ni_dmabuf_desc *dbuf_desc;
    unsigned long offset, length;
    int ret = 0;
    struct dma_buf *dmabuf;
    int domain, bus, devfn;
    struct pci_dev *pci_dev;
    int bar;

    if (copy_from_user(&exp, uexp, sizeof(exp)))
        return -EFAULT;

    bar = exp.bar;
    offset = exp.offset;
    length = exp.length;
    domain = exp.domain;
    bus = exp.bus;
    devfn = PCI_DEVFN(exp.dev, exp.fn);

    pci_dev = pci_get_domain_bus_and_slot(domain, bus, devfn);
    if (!pci_dev || pci_dev->vendor != NETINT_PCIDEV_VENDOR_ID) {
        ni_err("cannot find pci device: %04x:%02x:%02x.%01x\n",
                domain, bus, exp.dev, exp.fn);
        return -ENODEV;
    }

    if ((offset + length) > pci_resource_len(pci_dev, bar) || \
            ((offset + length) <= offset) || \
            (offset & (NI_DMA_BUF_PAGE_SIZE - 1)) || \
            (length & (NI_DMA_BUF_PAGE_SIZE - 1))) {
        ni_err("invalid pci params, offset 0x%lx, length 0x%lx, bar %d\n",
                offset, length, bar);
        pci_dev_put(pci_dev);
        return -EINVAL;
    }

    dbuf_desc = ni_dmabuf_desc_alloc(pci_dev, bar, offset, length);
    if (!dbuf_desc) {
        pci_dev_put(pci_dev);
        return -ENOMEM;
    }

    dmabuf = ni_dmabuf_export(dbuf_desc);
    if (IS_ERR(dmabuf)) {
        ni_err("failed to export dma buffer, dev %d\n", dev->devid);
        ni_dmabuf_desc_put(dbuf_desc);
        return PTR_ERR(dmabuf);
    }

    ret = dma_buf_fd(dmabuf, O_CLOEXEC);
    if (ret < 0) {
        ni_err("failed to get dmabuf fd, dev %d\n", dev->devid);
        goto put_dma_buf;
    }

    exp.fd = ret;
    if (copy_to_user(uexp, &exp, sizeof(exp))) {
        ret = -EFAULT;
        goto put_dma_buf;
    }
    return 0;

put_dma_buf:
    dma_buf_put(dmabuf);
    return ret;
}

#ifdef A1_IMPORT_FUNCTION

extern int amdgpu_dma_addr_get(struct dma_buf *dmabuf, phys_addr_t *dma_addr);
extern void amdgpu_dma_addr_put(struct dma_buf *dmabuf);

/*
 * This is a special version of the dma-buf import function
 * for customer A1 (mobile cloud gaming). Do not remove.
 */
static int ni_dev_dma_import_dmabuf_A1(struct ni_dev *dev,
        struct netint_iocmd_import_dmabuf __user *uimp)
{
    struct netint_iocmd_import_dmabuf *imp;
    int fd, flags, ret;
    int for_each_count = 0;
    struct dma_buf *dmabuf;
    phys_addr_t addr = NULL;

    imp = kzalloc(sizeof(struct netint_iocmd_import_dmabuf), GFP_KERNEL);
    if (!imp)
        return -ENOMEM;

    if (copy_from_user(imp, uimp, sizeof(*imp)))
        kfree(imp);
        return -EFAULT;

    fd = imp->fd;
    flags = imp->flags;
    mutex_lock(&dev->mutex);

    /* flags == 0 will import the dma-buf */
    if (flags == 0) {
        /* limit one dmabuf per device */
        if (dev->dmabuf) {
            ni_err("ni_dev has dmabuf\n");
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        dmabuf = dma_buf_get(fd);
        if (IS_ERR_OR_NULL(dmabuf)) {
            ni_err("failed to get dmabuf for import, dev %d, fd %d\n",
                   dev->devid, fd);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        ret = amdgpu_dma_addr_get(dmabuf, &addr);
        if (IS_ERR_OR_NULL(ret)) {
            dma_buf_put(dmabuf);
            ni_err("failed to get dmaaddr for import, ret %d\n", ret);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EFAULT;
        }

        imp->dma_addr[0] = (__u64) addr;
        /* The length of the transfer must be set in user-space */
        /* imp->dma_len[0] = xxxx; */
        imp->nents = 1;

        dev->dmabuf = dmabuf;
        if (copy_to_user(uimp, imp, sizeof(*imp))) {
            amdgpu_dma_addr_put(dmabuf);
            dma_buf_put(dmabuf);
            dev->dmabuf = NULL;
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EFAULT;
        }
    } else {
        /* flags !=0 will clean up the dma-buf */
        if (dev->dmabuf == NULL) {
            ni_err("no dmabuf\n");
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        amdgpu_dma_addr_put(dev->dmabuf);
        dma_buf_put(dev->dmabuf);
        dev->dmabuf = NULL;
    }

    mutex_unlock(&dev->mutex);
    kfree(imp);
    return 0;
}
#endif

static int ni_dev_dma_import_dmabuf(struct ni_dev *dev,
        struct netint_iocmd_import_dmabuf __user *uimp)
{
    struct netint_iocmd_import_dmabuf *imp;
    int domain, bus, devfn, fd, flags, i;
    struct dma_buf *dmabuf;
    struct pci_dev *pci_dev;
    struct dma_buf_attachment *dmabuf_attach;
    struct sg_table *sgtable;
    struct scatterlist *sg;

    imp = kzalloc(sizeof(struct netint_iocmd_import_dmabuf), GFP_KERNEL);
    if (!imp)
        return -ENOMEM;

    if (copy_from_user(imp, uimp, sizeof(*imp))) {
        kfree(imp);
        return -EFAULT;
    }

    fd = imp->fd;
    flags = imp->flags;
    domain = imp->domain;
    bus = imp->bus;
    devfn = PCI_DEVFN(imp->dev, imp->fn);

    mutex_lock(&dev->mutex);

    /* flags == 0 will import the dma-buf */
    if (flags == 0) {
        struct ni_dma_buf_info *dma_acquire = NULL;
        for(unsigned int dma_buf_index = 0; dma_buf_index < MAX_DMA_BUF_PER_DEV; dma_buf_index++) {   
            if (dev->dma_bufs[dma_buf_index].fd == -1) {
                dma_acquire = &dev->dma_bufs[dma_buf_index];
            }
        }  
        if (dma_acquire == NULL) {
            ni_err("ni_dev has maximum number of dmabuffers\n");
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        dmabuf = dma_buf_get(fd);
        if (IS_ERR_OR_NULL(dmabuf)) {
            ni_err("failed to get dmabuf for import, dev %d, fd %d\n",
                   dev->devid, fd);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        /* Get source PCI device */
        pci_dev = pci_get_domain_bus_and_slot(domain, bus, devfn);
        if (!pci_dev || pci_dev->vendor != NETINT_PCIDEV_VENDOR_ID) {

            ni_err("cannot find pci device: %04x:%02x:%02x.%01x\n",
                   domain, bus, imp->dev, imp->fn);
            dma_buf_put(dmabuf);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -ENODEV;
        }

        /* Attach the source PCI device to the dma buf */
        dmabuf_attach = dma_buf_attach(dmabuf, &pci_dev->dev);

        if (IS_ERR(dmabuf_attach)) {
            ni_err("failed to attach dmabuf\n");
            pci_dev_put(pci_dev);
            dma_buf_put(dmabuf);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return PTR_ERR(dmabuf_attach);
        }

        /* Map the attachment to generate the sg table */
        sgtable = dma_buf_map_attachment(dmabuf_attach, DMA_BIDIRECTIONAL);

        if (IS_ERR(sgtable)) {
            ni_err("failed to map attachment\n");
            dma_buf_detach(dmabuf, dmabuf_attach);
            pci_dev_put(pci_dev);
            dma_buf_put(dmabuf);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return PTR_ERR(sgtable);
        }

#ifdef NETINT_VERIFY_DMABUF
        if (ni_debug_level > 0) {
            int i;
            struct scatterlist *sg;

            for_each_sgtable_dma_sg(sgtable, sg, i) {
                ni_debug("dma_addr %llx len %d\n",
                         sg_dma_address(sg), sg_dma_len(sg));
            }
        }
#endif
        /* Check we don't exceed 128 entries */
        if (sgtable->nents > NI_DMABUF_MAX_SGL_ENTRY) {
            ni_err("too many sg table entries %d\n", sgtable->nents);
            dma_buf_unmap_attachment(dmabuf_attach, sgtable, DMA_BIDIRECTIONAL);
            dma_buf_detach(dmabuf, dmabuf_attach);
            pci_dev_put(pci_dev);
            dma_buf_put(dmabuf);
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -ENOMEM;
        }

        for_each_sgtable_dma_sg(sgtable, sg, i) {
            imp->dma_addr[i] = sg_dma_address(sg);
            imp->dma_len[i] = sg_dma_len(sg);
        }

        imp->nents = sgtable->nents;

        dma_acquire->sgtable = sgtable;
        dma_acquire->dmabuf_attach = dmabuf_attach;
        dma_acquire->pci_dev = pci_dev;
        dma_acquire->dmabuf = dmabuf;
        dma_acquire->fd = fd;

        if (copy_to_user(uimp, imp, sizeof(*imp))) {
            dma_buf_unmap_attachment(dmabuf_attach, sgtable, DMA_BIDIRECTIONAL);
            dma_buf_detach(dmabuf,dmabuf_attach);
            pci_dev_put(pci_dev);
            dma_buf_put(dmabuf);
            dma_acquire->pci_dev = NULL;
            dma_acquire->sgtable = NULL;
            dma_acquire->dmabuf_attach = NULL;
            dma_acquire->dmabuf = NULL;
            dma_acquire->fd = -1;
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EFAULT;
        }
    } else {
        struct ni_dma_buf_info *dma_release = NULL;
        for(unsigned int dma_buf_index = 0; dma_buf_index < MAX_DMA_BUF_PER_DEV; dma_buf_index++) {   
            if (dev->dma_bufs[dma_buf_index].fd == fd) {
                dma_release = &dev->dma_bufs[dma_buf_index];
            }
        }        
        /* flags !=0 will clean up the dma-buf */
        if (dma_release == NULL) {
            ni_err("no dmabuf\n");
            mutex_unlock(&dev->mutex);
            kfree(imp);
            return -EINVAL;
        }

        pci_dev_put(dma_release->pci_dev);
        dma_release->pci_dev = NULL;

        dma_buf_unmap_attachment(dma_release->dmabuf_attach, dma_release->sgtable, DMA_BIDIRECTIONAL);
        dma_release->sgtable = NULL;

        dma_buf_detach(dma_release->dmabuf, dma_release->dmabuf_attach);
        dma_release->dmabuf_attach = NULL;

        dma_buf_put(dma_release->dmabuf);
        dma_release->dmabuf = NULL;
        //Clear the handle
        dma_release->fd = -1;
    }

    mutex_unlock(&dev->mutex);
    kfree(imp);
    return 0;
}

static int ni_dev_attach_read_fence(struct ni_dev *dev,
        struct netint_iocmd_attach_rfence *uatch)
{
    struct netint_iocmd_attach_rfence atch;
    int ret = 0;
    int fd;
    unsigned int flags;
    struct ni_fence *ni_fence = NULL;
//    int out_fd = -1;

    if (copy_from_user(&atch, uatch, sizeof(atch)))
        return -EFAULT;

    flags = atch.flags;
    fd = atch.fd;
    if (fd < 0) {
        ni_err("%s: invalid dmabuf fd %d, dev %d\n", __func__,
                fd, dev->devid);
        return -EINVAL;
    }
    {
        ni_fence = ni_fence_alloc();
        if (!ni_fence) {
            ni_err("failed to allocate fence for fd %d, dev %d\n",
                    fd, dev->devid);
            return -ENOMEM;
        }

        ret = ni_dmabuf_attach_fence(dev, fd, ni_fence, 0);
        if (ret < 0) {
            ni_err("failed to attach read fence for fd %d, dev %d\n",
                    fd, dev->devid);
            ni_fence_free(ni_fence);
            return ret;
        }
    }
    return ret;
}

static int ni_dev_signal_read_fence(struct ni_dev *dev,
        struct netint_iocmd_signal_rfence *usigl)
{
    struct netint_iocmd_signal_rfence sigl;
    int ret;
    int fd;

    if (copy_from_user(&sigl, usigl, sizeof(sigl)))
        return -EFAULT;

    fd = sigl.fd;
    if (fd < 0)
        return -EINVAL;

    ret = ni_dmabuf_signal_fence(dev, fd, 0);
    if (ret < 0)
        ni_err("failed to signal read fence for fd %d, dev %d\n",
                fd, dev->devid);
    return ret;
}

#ifdef NETINT_VERIFY_DMABUF
static int ni_dma_issue_req(struct ni_dev *dev,
        struct netint_iocmd_issue_request __user *uissue)
{
    struct netint_iocmd_issue_request issue;
    struct ni_ioreq *req;

    if (copy_from_user(&issue, uissue,
            sizeof(struct netint_iocmd_issue_request)))
        return -EFAULT;

    if (!issue.data || issue.len == 0 || issue.fd < 0)
        return -EINVAL;

    mutex_lock(&dev->req_timeline->mutex);
    req = ni_alloc_io_request(dev, issue.fd, issue.data, issue.len,
            issue.dir == NI_DMABUF_WRITE_TO_DEVICE);
    if (IS_ERR(req)) {
        ni_err("failed to allocate request: dev %d, fd %d\n",
                dev->devid, issue.fd);
        mutex_unlock(&dev->req_timeline->mutex);
        return PTR_ERR(req);
    }

    ni_reschedule_ioreq(dev, req);
    mutex_unlock(&dev->req_timeline->mutex);
    return 0;
}
#endif

static int ni_dev_attach_write_fence(struct ni_dev *dev,
        struct netint_iocmd_attach_wfence *uatch)
{
    struct netint_iocmd_attach_wfence atch;
    int ret = 0;
    int fd;
    unsigned int flags;
    struct ni_fence *ni_fence = NULL;

    if (copy_from_user(&atch, uatch, sizeof(atch)))
        return -EFAULT;

    flags = atch.flags;
    fd = atch.fd;
    if (fd < 0) {
        ni_err("%s: invalid dmabuf fd %d, dev %d\n", __func__,
                fd, dev->devid);
        return -EINVAL;
    }

    ni_fence = ni_fence_alloc();
    if (!ni_fence) {
        ni_err("failed to allocate fence for fd %d, dev %d\n",
                fd, dev->devid);
        return -ENOMEM;
    }

    ret = ni_dmabuf_attach_fence(dev, fd, ni_fence, 1);
    if (ret < 0) {
        ni_err("failed to attach write fence for fd %d, dev %d\n",
                fd, dev->devid);
        ni_fence_free(ni_fence);
        return ret;
    }

    return ret;
}

static int ni_dev_signal_write_fence(struct ni_dev *dev,
        struct netint_iocmd_signal_wfence *usigl)
{
    struct netint_iocmd_signal_wfence sigl;
    int ret;
    int fd;

    if (copy_from_user(&sigl, usigl, sizeof(sigl)))
        return -EFAULT;

    fd = sigl.fd;
    if (fd < 0)
        return -EINVAL;

    ret = ni_dmabuf_signal_fence(dev, fd, 1);
    if (ret < 0)
        ni_err("failed to signal write fence for fd %d, dev %d\n",
                fd, dev->devid);
    return ret;
}

static long ni_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    struct ni_dev *dev = (struct ni_dev *)file->private_data;

    switch (cmd) {
    case NETINT_IOCTL_EXPORT_DMABUF:
        return ni_dev_dma_export_dmabuf(dev, (void __user *)arg);
    case NETINT_IOCTL_ATTACH_RFENCE:
        return ni_dev_attach_read_fence(dev, (void __user *)arg);
    case NETINT_IOCTL_SIGNAL_RFENCE:
        return ni_dev_signal_read_fence(dev, (void __user *)arg);
    case NETINT_IOCTL_ATTACH_WFENCE:
        return ni_dev_attach_write_fence(dev, (void __user *)arg);
    case NETINT_IOCTL_SIGNAL_WFENCE:
        return ni_dev_signal_write_fence(dev, (void __user *)arg);
    case NETINT_IOCTL_IMPORT_DMABUF:
#ifdef A1_IMPORT_FUNCTION
        return ni_dev_dma_import_dmabuf_A1(dev, (void __user *)arg);
#else
        return ni_dev_dma_import_dmabuf(dev, (void __user *)arg);
#endif
#ifdef NETINT_VERIFY_DMABUF
    case NETINT_IOCTL_ISSUE_REQ:
        return ni_dma_issue_req(dev, (void __user *)arg);
#endif
    default:
        return -ENOTTY;
    }
}

static struct file_operations ni_fops = {
    .owner = THIS_MODULE,
    .open  = ni_open,
    .release = ni_release,
    .unlocked_ioctl = ni_ioctl,
    .compat_ioctl = ni_ioctl,
};

static int __init netint_module_init(void)
{
    int ret;

    dev_t dev = MKDEV(netint_major, 0);
    if (netint_major)
        ret = register_chrdev_region(dev, 1, "netint");
    else {
        ret = alloc_chrdev_region(&dev, 0, 1, "netint");
        netint_major = MAJOR(dev);
    }
    if (ret < 0) {
        ni_err("failed to allocate major\n");
        return ret;
    }
    if (netint_major == 0)
        netint_major = ret;

#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 4, 0)
    netint_class = class_create(THIS_MODULE, "netint");
#else
    netint_class = class_create("netint");
#endif
    if (IS_ERR(netint_class)) {
        ret = PTR_ERR(netint_class);
        goto unregister_chrdev;
    }

    dev = MKDEV(netint_major, 0);
    cdev_init(&ni_cdev, &ni_fops);
    ni_cdev.owner = THIS_MODULE;
    ni_cdev.ops = &ni_fops;
    ret = cdev_add(&ni_cdev, dev, 1);
    if (ret < 0) {
        ni_err("failed to add cdev\n");
        goto destroy_class;
    }

    netint_device = device_create(netint_class, NULL, dev, NULL, "netint");
    if (IS_ERR(netint_device)) {
        ni_err("failed to create device\n");
        goto del_cdev;
    }

    ret = ni_fence_init();
    if (ret < 0) {
        ni_err("failed to initialize ni fence\n");
        goto del_dev;
    }

    ret = ni_ioreq_init();
    if (ret < 0) {
        ni_err("failed to create ioreq cache\n");
        goto cleanup_fence;
    }

    ret = ni_debugfs_init();
    if (ret < 0) {
        ni_err("failed to initialize debugfs\n");
        goto cleanup_ioreq;
    }

    ret = ni_dmabuf_desc_slab_init();
    if (ret < 0) {
        ni_err("failed to initialize dmabuf_desc slab\n");
        goto cleanup_debugfs;
    }

    ida_init(&ni_dev_ida);

    return 0;
cleanup_debugfs:
    ni_debugfs_exit();
cleanup_ioreq:
    ni_ioreq_fini();
cleanup_fence:
    ni_fence_cleanup();
del_dev:
    device_destroy(netint_class, dev);
del_cdev:
    cdev_del(&ni_cdev);
destroy_class:
    class_destroy(netint_class);
unregister_chrdev:
    unregister_chrdev_region(dev, 1);
    return ret;
}

static void netint_module_exit(void)
{
    dev_t dev = MKDEV(netint_major, 0);

    ida_destroy(&ni_dev_ida);
    ni_dmabuf_desc_slab_exit();
    ni_debugfs_exit();
    ni_ioreq_fini();

    ni_fence_cleanup();

    device_destroy(netint_class, dev);
    cdev_del(&ni_cdev);
    class_destroy(netint_class);
    unregister_chrdev_region(dev, 1);
}

module_init(netint_module_init);
module_exit(netint_module_exit);
MODULE_LICENSE("GPL v2");
MODULE_VERSION("0.0.7");
MODULE_DESCRIPTION("netint dmabuf driver");
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 15, 0)
MODULE_IMPORT_NS(DMA_BUF);
#endif
