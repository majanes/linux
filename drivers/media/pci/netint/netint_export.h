#ifndef _NETINT_EXPORT_H
#define _NETINT_EXPORT_H

#include "netint.h"

extern struct dma_buf *ni_dmabuf_export(struct ni_dmabuf_desc *buf_desc);
extern void ni_free_fence(struct ni_dev *ni_dev, struct dma_fence *fence);
extern int ni_dmabuf_attach_fence(struct ni_dev *ni_dev, int fd,
        struct ni_fence *ni_fence, int exclusive);
extern int ni_dmabuf_signal_fence(struct ni_dev *ni_dev, int fd, int exclusive);
#endif
