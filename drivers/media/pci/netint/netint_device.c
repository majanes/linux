#include "netint_device.h"

static struct kmem_cache *ni_dmabuf_desc_cache;
atomic64_t ni_dmabuf_desc_count;

void ni_dmabuf_desc_put(struct ni_dmabuf_desc *dbuf_desc)
{
    if (atomic_dec_and_test(&dbuf_desc->refcount)) {
        if (dbuf_desc->vaddr)
            iounmap(dbuf_desc->vaddr);
        if (dbuf_desc->pci_dev)
            pci_dev_put(dbuf_desc->pci_dev);
        kmem_cache_free(ni_dmabuf_desc_cache, dbuf_desc);
        atomic64_dec(&ni_dmabuf_desc_count);
    }
}

struct ni_dmabuf_desc *ni_dmabuf_desc_alloc(struct pci_dev *pci_dev,
        int bar, unsigned long offset, unsigned long length)
{
    struct ni_dmabuf_desc *dbuf_desc;

    dbuf_desc = kmem_cache_alloc(ni_dmabuf_desc_cache, GFP_KERNEL);
    if (!dbuf_desc) {
        ni_err("failed to allocate dmabuf desc\n");
        return NULL;
    }
    memset(dbuf_desc, 0, sizeof(*dbuf_desc));

    dbuf_desc->pci_dev = pci_dev;
    dbuf_desc->flags = pci_dev->resource[bar].flags;
    dbuf_desc->phys_addr = pci_resource_start(pci_dev, bar) + offset;
    dbuf_desc->len = length;
    INIT_LIST_HEAD(&dbuf_desc->list);

    dbuf_desc->vaddr = ioremap(dbuf_desc->phys_addr, length);
    if (!dbuf_desc->vaddr) {
        ni_err("failed to map phy addr\n");
        goto fail_out;
    }

    atomic_set(&dbuf_desc->refcount, 1);
    atomic64_inc(&ni_dmabuf_desc_count);

    return dbuf_desc;
fail_out:
    if (dbuf_desc->vaddr)
        iounmap(dbuf_desc->vaddr);
    kmem_cache_free(ni_dmabuf_desc_cache, dbuf_desc);
    return NULL;
}

int ni_dmabuf_desc_slab_init(void)
{
    ni_dmabuf_desc_cache = kmem_cache_create("ni_dmabuf_desc",
            sizeof(struct ni_dmabuf_desc), 0, SLAB_HWCACHE_ALIGN, NULL);
    if (!ni_dmabuf_desc_cache) {
        ni_err("failed to create dmabuf desc cache\n");
        return -ENOMEM;
    }

    atomic64_set(&ni_dmabuf_desc_count, 0);
    return 0;
}

void ni_dmabuf_desc_slab_exit(void)
{
    if (unlikely(atomic64_read(&ni_dmabuf_desc_count) != 0))
        ni_err("ni_dmabuf_desc %ld isn't zero!!!\n",
                (long)atomic64_read(&ni_dmabuf_desc_count));
    kmem_cache_destroy(ni_dmabuf_desc_cache);
}
