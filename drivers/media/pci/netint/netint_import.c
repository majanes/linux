#include "netint.h"
#include "netint_import.h"

static void ni_import_unmap_dmabuf(struct ni_dmabuf_desc *dbuf_desc)
{
    if (!dbuf_desc->db_attach)
        return;

    if (!dbuf_desc->sgtable)
        return;

    if (dbuf_desc->vaddr) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 18, 0)
        struct iosys_map map = IOSYS_MAP_INIT_VADDR(dbuf_desc->vaddr);
        dma_buf_vunmap(dbuf_desc->db_attach->dmabuf, &map);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 11, 0)
        struct dma_buf_map map = DMA_BUF_MAP_INIT_VADDR(dbuf_desc->vaddr);
        dma_buf_vunmap(dbuf_desc->db_attach->dmabuf, &map);
#else
        dma_buf_vunmap(dbuf_desc->db_attach->dmabuf, dbuf_desc->vaddr);
#endif
        dbuf_desc->vaddr = NULL;
    }
    dma_buf_unmap_attachment(dbuf_desc->db_attach, dbuf_desc->sgtable, dbuf_desc->dma_dir);
    dbuf_desc->sgtable = NULL;
}

static int ni_import_map_dmabuf(struct ni_dmabuf_desc *dbuf_desc)
{
    struct sg_table *sgtable;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 18, 0)
    struct iosys_map map;
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 11, 0)
    struct dma_buf_map map;
#endif

    if (!dbuf_desc->db_attach)
        return -EINVAL;

    if (dbuf_desc->sgtable)
        return 0;

    sgtable = dma_buf_map_attachment(dbuf_desc->db_attach, dbuf_desc->dma_dir);
    if (IS_ERR(sgtable)) {
        ni_err("failed to map attachment\n");
        return PTR_ERR(sgtable);
    }

    dbuf_desc->sgtable = sgtable;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 11, 0)
    dbuf_desc->vaddr = dma_buf_vmap(dbuf_desc->db_attach->dmabuf, &map) ? NULL : map.vaddr;
#else
    dbuf_desc->vaddr = dma_buf_vmap(dbuf_desc->db_attach->dmabuf);
#endif
    ni_debug("map dmabuf: dma_address 0x%016llx, vaddr 0x%016lx\n",
            sg_dma_address(sgtable->sgl), (unsigned long)dbuf_desc->vaddr);
    return 0;
}

static struct ni_dmabuf_desc *ni_import_attach_dmabuf(struct pci_dev *pci_dev,
        struct dma_buf *dmabuf, enum dma_data_direction dma_dir)
{
    struct ni_dmabuf_desc *dbuf_desc;
    struct dma_buf_attachment *dmabuf_attach;

    dbuf_desc = kzalloc(sizeof(struct ni_dmabuf_desc), GFP_KERNEL);
    if (!dbuf_desc)
        return ERR_PTR(-ENOMEM);

    dbuf_desc->pci_dev = pci_dev;

    dmabuf_attach = dma_buf_attach(dmabuf, &dbuf_desc->pci_dev->dev);
    if (IS_ERR(dmabuf_attach)) {
        ni_err("failed to attach dmabuf\n");
        kfree(dbuf_desc);
        return (struct ni_dmabuf_desc *)dmabuf_attach;
    }

    dbuf_desc->len = dmabuf->size;
    dbuf_desc->db_attach = dmabuf_attach;
    dbuf_desc->dma_dir = dma_dir;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    dma_resv_init(&dbuf_desc->resv);
#else
    reservation_object_init(&dbuf_desc->resv);
#endif
    ni_debug("attach dmabuf: len 0x%x\n", dbuf_desc->len);

    return dbuf_desc;
}

static void ni_import_detach_dmabuf(struct ni_dmabuf_desc *dbuf_desc)
{
    if (dbuf_desc->sgtable)
        ni_import_unmap_dmabuf(dbuf_desc);

    dma_buf_detach(dbuf_desc->db_attach->dmabuf, dbuf_desc->db_attach);
    kfree(dbuf_desc);
}

struct ni_dmabuf_desc *ni_dmabuf_import(struct dma_buf *dmabuf)
{
    struct ni_dmabuf_desc *dbuf_desc;
    int ret;

    /* attach dmabuf */
    dbuf_desc = ni_import_attach_dmabuf(
            ((struct ni_dmabuf_desc *)(dmabuf->priv))->pci_dev,
            dmabuf, DMA_BIDIRECTIONAL);
    if (IS_ERR(dbuf_desc)) {
        ni_err("failed to attach dmabuf\n");
        return dbuf_desc;
    }

    /* map dmabuf */
    ret = ni_import_map_dmabuf(dbuf_desc);
    if (ret < 0) {
        ni_err("failed to map dmabuf\n");
        ni_import_detach_dmabuf(dbuf_desc);
        return ERR_PTR(ret);
    }

    /* dma_buf->resv must exist here because the exporter promise it. */
    if (unlikely(!dmabuf->resv)) {
        ni_warn("no resv in the dmabuf!!!\n");
        dbuf_desc->presv = NULL;
    } else
        dbuf_desc->presv = dmabuf->resv;

    return dbuf_desc;
}

void ni_dmabuf_import_free(struct ni_dmabuf_desc *dbuf_desc)
{
    struct dma_buf *dmabuf = NULL;

    if (!dbuf_desc)
        return;

    if (dbuf_desc->db_attach && dbuf_desc->db_attach->dmabuf)
        dmabuf = dbuf_desc->db_attach->dmabuf;

    ni_import_unmap_dmabuf(dbuf_desc);
    ni_import_detach_dmabuf(dbuf_desc);
    if (dmabuf)
        dma_buf_put(dmabuf);
}

