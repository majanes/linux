#ifndef _NETINT_H
#define _NETINT_H

#include <linux/list.h>
#include <linux/pci.h>
#include <linux/scatterlist.h>
#include <linux/dma-buf.h>
#include <linux/device.h>
#include <linux/types.h>

#include <linux/version.h>

#include <linux/workqueue.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>

#include <linux/dma-fence.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
#include <linux/dma-resv.h>
#else
#include <linux/reservation.h>
#endif

#define NETINT_PCIDEV_VENDOR_ID  0x1D82U

#define NI_DMA_BUF_PAGE_SIZE   4096UL

#define MAX_DMA_BUF_PER_DEV 6

extern int ni_debug_level;
#define ni_debug(fmt, arg...) \
    do {    \
        if (ni_debug_level > 0) \
            printk(KERN_ERR "ni_dbg: " fmt, ##arg);    \
    } while (0)

#define ni_info(fmt, arg...) \
    printk(KERN_ERR "ni_info: " fmt, ##arg)
#define ni_warn(fmt, arg...) \
    printk(KERN_ERR "ni_warn: " fmt, ##arg)
#define ni_err(fmt, arg...) \
    printk(KERN_ERR "ni_err: "  fmt, ##arg)
#define ni_fatal(fmt, arg...) \
    printk(KERN_ERR "ni_emg: "  fmt, ##arg)

struct ni_dmabuf_attachment {
    struct device *dev;
    dma_addr_t dma_handle;
    struct pci_dev *src_dev;
};

struct ni_timeline {
    u64 context;
    u64 seqno;
    u64 hwseqno;
    atomic_t refcount;
    struct mutex mutex;
    spinlock_t lock;
};

struct ni_fence {
    struct dma_fence fence;
    struct ni_timeline *timeline;
    struct list_head list;
};

struct ni_dma_buf_info {
    struct dma_buf *dmabuf;
    struct pci_dev *pci_dev;
    struct dma_buf_attachment *dmabuf_attach;
    struct sg_table *sgtable;
    int fd;
};

struct ni_dev {
#define NETINT_NI_DEV_NAME 32
    char name[NETINT_NI_DEV_NAME];
    unsigned int devid;
    struct ni_timeline *timeline;
    struct list_head fences;
    struct ni_dma_buf_info dma_bufs[MAX_DMA_BUF_PER_DEV];
    spinlock_t fence_lock;
    struct mutex mutex;
#ifdef NETINT_VERIFY_DMABUF
    struct workqueue_struct *bg_dma_wq;
    struct ni_timeline *req_timeline;
    struct work_struct dma_work;
    spinlock_t req_lock;
    struct list_head req_list;
#endif
};

struct ni_dmabuf_desc {
    struct ni_dev *ni_dev;
    struct pci_dev *pci_dev;
    unsigned long phys_addr;
    void __iomem *vaddr;
    uint32_t len;
    int dma_dir;
    atomic_t refcount;
    unsigned long flags;
    struct list_head list;
    struct ni_dmabuf_attachment *attach;
    struct sg_table *sgtable;
    struct dma_buf_attachment *db_attach;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    struct dma_resv *presv;
    struct dma_resv resv;
#else
    struct reservation_object *presv;
    struct reservation_object resv;
#endif
};

#endif /* _NETINT_H */
