#ifndef _NETINT_IOREQ_H
#define _NETINT_IOREQ_H

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 8, 0)
#include <linux/bvec.h>
#else
#include <linux/blk_types.h>
#endif
#include "netint.h"
#include "netint_import.h"

struct ni_ioreq {
    struct list_head list;
    struct bio_vec *io_vec;
    unsigned int vcnt;
    int dir;
    unsigned long len;
    struct ni_dmabuf_desc *dbuf_desc;
    struct ni_fence *ni_fence;
};

#ifdef NETINT_VERIFY_DMABUF
extern int ni_dev_init_ioreq(struct ni_dev *dev);
extern void ni_dev_free_ioreq(struct ni_dev *dev);
extern void ni_free_io_request(struct ni_ioreq *req);
extern struct ni_ioreq *ni_alloc_io_request(struct ni_dev *dev, int fd,
        void __user *data, unsigned long len, int exclusive);
extern int ni_reschedule_ioreq(struct ni_dev *dev, struct ni_ioreq *req);
extern int ni_ioreq_init(void);
extern void ni_ioreq_fini(void);
#else
static inline int ni_dev_init_ioreq(struct ni_dev *dev)
{
    return 0;
}

static inline void ni_dev_free_ioreq(struct ni_dev *dev)
{
}

static inline void ni_free_io_request(struct ni_dev *dev, struct ni_ioreq *req)
{
}

static inline struct ni_ioreq *ni_alloc_io_request(struct ni_dev *dev, int fd,
 void __user *data, unsigned long len, int exclusive)
{
    return ERR_PTR(-EINVAL);
}

static inline int ni_reschedule_ioreq(struct ni_dev *dev, struct ni_ioreq *req)
{
    return -EINVAL;
}

static inline int ni_ioreq_init(void)
{
    return 0;
}

static inline void ni_ioreq_fini(void)
{
}
#endif
#endif
