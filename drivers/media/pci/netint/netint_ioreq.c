#include <linux/uaccess.h>
#include <linux/highmem.h>
#include <linux/dma-fence-array.h>
#include "netint.h"
#include "netint_ioreq.h"
#include "netint_fence.h"

#ifdef NETINT_VERIFY_DMABUF
static struct kmem_cache *ni_ioreq_cache;

atomic64_t ni_ioreq_count;

void ni_free_io_request(struct ni_ioreq *req)
{
    if (req->io_vec) {
        int i;
        for (i = 0; i < req->vcnt; i++) {
            if (req->io_vec[i].bv_page) {
                put_page(req->io_vec[i].bv_page);
                req->io_vec[i].bv_page = NULL;
            }
        }
        kfree(req->io_vec);
    }

    if (req->dbuf_desc) {
        if (req->dbuf_desc->presv == &req->dbuf_desc->resv) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
            dma_resv_fini(req->dbuf_desc->presv);
#else
            reservation_object_fini(req->dbuf_desc->presv);
#endif
        }
        ni_dmabuf_import_free(req->dbuf_desc);
    }

    kmem_cache_free(ni_ioreq_cache, req);
    atomic64_dec(&ni_ioreq_count);
}

static int iter_io_request(struct ni_ioreq *req, void __user *data,
        unsigned long len, int write)
{
    unsigned long offset;
    unsigned long addr;
    int count = 0;
    struct page **pages;
    int ret;
    int i;
    struct bio_vec *io_vec;

    if ((len & (PAGE_SIZE - 1)) || ((unsigned long)data & (PAGE_SIZE - 1)))
        return -EINVAL;

    addr = (unsigned long)data;
    offset = offset_in_page(addr);
    count = DIV_ROUND_UP(offset + len, PAGE_SIZE);
    pages = kcalloc(count, sizeof(struct page *), GFP_KERNEL);
    if (!pages)
        return -ENOMEM;

    ret = get_user_pages_fast(addr, count, write ? 0 : FOLL_WRITE, pages);
    if (ret < 0 || (ret < count)) {
        ni_err("%s: failed to get user page, dir %d. ret %d, expect %d\n",
                __func__, write, ret, count);
        if (ret >= 0) {
            count = ret;
            ret = -EFAULT;
        }
        goto put_pages;
    }

    io_vec = kzalloc(sizeof(struct bio_vec) * count, GFP_KERNEL);
    if (!io_vec) {
        ni_err("failed to allocate io vec\n");
        ret = -ENOMEM;
        goto put_pages;
    }

    req->len = len;
    req->vcnt = count;
    for (i = 0; i < count; i++) {
        io_vec[i].bv_page = pages[i];
        io_vec[i].bv_len = min_t(unsigned, len, PAGE_SIZE - offset);
        io_vec[i].bv_offset = offset;
        len -= (PAGE_SIZE - offset);
        offset = 0;
    }
    req->io_vec = io_vec;
    req->dir = write ? DMA_TO_DEVICE : DMA_FROM_DEVICE;

    kfree(pages);
    return 0;

put_pages:
    for (i = 0; i < count; i++)
        put_page(pages[i]);
    kfree(pages);
    return ret;
}

static const char *ni_dma_req_fence_get_driver_name(struct dma_fence *fence)
{
    return "ni_dev";
}

static const char *ni_dma_req_fence_get_timeline_name(struct dma_fence *fence)
{
    if (test_bit(DMA_FENCE_FLAG_SIGNALED_BIT, &fence->flags))
        return "signaled";

    return "ni_dma_fence";
}

static bool ni_dma_req_fence_enable_signaling(struct dma_fence *fence)
{
    return true;
}

static inline bool ni_fence_passed(u64 seq1, u64 seq2)
{
    return (s64)(seq1 - seq2) >= 0;
}

static bool ni_dma_req_fence_signaled(struct dma_fence *fence)
{
    struct ni_fence *ni_fence = container_of(fence, struct ni_fence, fence);

    if (test_bit(DMA_FENCE_FLAG_SIGNALED_BIT, &fence->flags))
        return true;

    if (ni_fence_passed(ni_fence->timeline->hwseqno, ni_fence->fence.seqno))
        return true;

    return false;
}

static void ni_dma_req_fence_release(struct dma_fence *fence)
{
    struct ni_fence *ni_fence = container_of(fence, struct ni_fence, fence);

    if (ni_fence->timeline)
        ni_timeline_put(ni_fence->timeline);
    ni_fence_free(ni_fence);
}

static const struct dma_fence_ops ni_dma_req_fence_ops = {
    .get_driver_name = ni_dma_req_fence_get_driver_name,
    .get_timeline_name = ni_dma_req_fence_get_timeline_name,
    .enable_signaling = ni_dma_req_fence_enable_signaling,
    .signaled = ni_dma_req_fence_signaled,
    .wait = dma_fence_default_wait,
    .release = ni_dma_req_fence_release,
};

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
static int ni_ioreq_wait_fence(struct ni_dev *dev, struct dma_buf *dmabuf,
        int exclusive)
{
    int ret = 0;
    struct dma_resv_iter cursor;
    struct dma_fence *f;

    if (!dmabuf->resv)
        return 0;

    if (exclusive) {
        dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_READ);
    } else {
        dma_resv_iter_begin(&cursor, dmabuf->resv, DMA_RESV_USAGE_WRITE);
    }
    dma_resv_for_each_fence_unlocked(&cursor, f) {
        ni_debug("%s: wait fence, usage %d\n", __func__, cursor.usage);
        ret = dma_fence_wait(f, true);
        if (ret < 0) {
            ni_err("%s: failed to wait fence: usage %d\n", __func__, cursor.usage);
            dma_resv_iter_end(&cursor);
            return ret;
        }
    }
    dma_resv_iter_end(&cursor);
    return ret;
}
#else
static int ni_ioreq_wait_fence(struct ni_dev *dev, struct dma_buf *dmabuf,
        int exclusive)
{
    struct dma_fence **shared = NULL;
    int count;
    struct dma_fence *excl = NULL;
    int i = 0;
    int ret = 0;

    if (!dmabuf->resv)
        return 0;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 14, 0)
    ret = dma_resv_get_fences(dmabuf->resv,
            &excl, &count, &shared);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
    ret = dma_resv_get_fences_rcu(dmabuf->resv,
            &excl, &count, &shared);
#else
    ret = reservation_object_get_fences_rcu(dmabuf->resv,
            &excl, &count, &shared);
#endif
    if (ret < 0) {
        ni_err("%s: failed to get fence\n", __func__);
        return ret;
    }

    if (exclusive) {
        ni_debug("%s: shared fence count %d\n", __func__, count);
        for (i = 0; i < count; i++) {
            ret = dma_fence_wait(shared[i], true);
            dma_fence_put(shared[i]);
            if (ret < 0) {
                ni_err("%s: failed to wait read fence, id %d\n", __func__, i);
                goto out;
            }
        }
    }

    if (excl) {
        if (excl->ops != &ni_dma_req_fence_ops ||
                false == dma_fence_match_context(excl,
                        dev->req_timeline->context)) {
            ret = dma_fence_wait(excl, true);
            if (ret < 0)
                ni_err("failed to wait write fence\n");
        }
    }

out:
    if (shared) {
        for (i = i + 1; i < count; i++)
            dma_fence_put(shared[i]);
        kfree(shared);
    }
    if (excl)
        dma_fence_put(excl);
    return ret;
}
#endif

static inline struct ni_ioreq *alloc_ni_req(void)
{
    struct ni_ioreq *req = kmem_cache_alloc(ni_ioreq_cache, GFP_KERNEL);
    if (req) {
        memset(req, 0, sizeof(*req));
        INIT_LIST_HEAD(&req->list);
        atomic64_inc(&ni_ioreq_count);
    }

    return req;
}

struct ni_ioreq *ni_alloc_io_request(struct ni_dev *dev, int fd,
        void __user *data, unsigned long len, int exclusive)
{
    struct ni_ioreq *req;
    struct dma_buf *dmabuf;
    struct ni_dmabuf_desc *dbuf_desc;
    int ret;

    req = alloc_ni_req();
    if (!req) {
        ni_err("failed to allocate request. dev %d\n", dev->devid);
        return ERR_PTR(-ENOMEM);
    }

    ret = iter_io_request(req, data, len, exclusive);
    if (ret < 0) {
        ni_err("failed to iter io request. dev %d, fd %d\n",
                dev->devid, fd);
        goto fail_out;
    }

    req->ni_fence = ni_fence_alloc();
    if (!req->ni_fence) {
        ni_err("failed to allocate fence, fd %d\n", fd);
        goto fail_out;
    }

    dmabuf = dma_buf_get(fd);
    if (IS_ERR_OR_NULL(dmabuf)) {
        ni_err("failed to get dmabuf for ioreq, dev %d, fd %d\n",
                dev->devid, fd);
        ret = -EINVAL;
        goto fail_out;
    }

    /* wait for read fence to complete */
    ret = ni_ioreq_wait_fence(dev, dmabuf, exclusive);
    if (ret < 0) {
        ni_err("%s: failed to wait ioreq fence. fd %d\n",
                __func__, fd);
        goto put_dmabuf;
    }

    dbuf_desc = ni_dmabuf_import(dmabuf);
    if (IS_ERR(dbuf_desc)) {
        ni_err("failed to import dmabuf, fd %d\n", fd);
        ret = PTR_ERR(dbuf_desc);
        goto put_dmabuf;
    }

    ni_fence_init_context(req->ni_fence, &ni_dma_req_fence_ops,
            dev->req_timeline);
    if (likely(dbuf_desc->presv)) {
        if (exclusive) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
            dma_resv_lock(dbuf_desc->presv, NULL);
            ret = dma_resv_reserve_fences(dbuf_desc->presv, 1);
            if (ret) {
                ni_err("failed to reserve fence slot for write: fd %d\n", fd);
                dma_resv_unlock(dbuf_desc->presv);
                goto put_dmabuf;
            }
            dma_resv_add_fence(dbuf_desc->presv, &req->ni_fence->fence, DMA_RESV_USAGE_WRITE);
            dma_resv_unlock(dbuf_desc->presv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
            dma_resv_lock(dbuf_desc->presv, NULL);
            dma_resv_add_excl_fence(dbuf_desc->presv, &req->ni_fence->fence);
            dma_resv_unlock(dbuf_desc->presv);
#else
            reservation_object_lock(dbuf_desc->presv, NULL);
            reservation_object_add_excl_fence(dbuf_desc->presv,
                    &req->ni_fence->fence);
            reservation_object_unlock(dbuf_desc->presv);
#endif
        } else {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 19, 0)
            dma_resv_lock(dbuf_desc->presv, NULL);
            ret = dma_resv_reserve_fences(dbuf_desc->presv, 1);
            if (ret) {
                ni_err("failed to reserve fence slot for read: fd %d\n", fd);
                dma_resv_unlock(dbuf_desc->presv);
                goto put_dmabuf;
            }
            dma_resv_add_fence(dbuf_desc->presv, &req->ni_fence->fence, DMA_RESV_USAGE_READ);
            dma_resv_unlock(dbuf_desc->presv);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(5, 4, 0)
            dma_resv_lock(dbuf_desc->presv, NULL);
            dma_resv_add_shared_fence(dbuf_desc->presv, &req->ni_fence->fence);
            dma_resv_unlock(dbuf_desc->presv);
#else
            reservation_object_lock(dbuf_desc->presv, NULL);
            reservation_object_add_shared_fence(dbuf_desc->presv, &req->ni_fence->fence);
            reservation_object_unlock(dbuf_desc->presv);
#endif
        }
    }
    req->dbuf_desc = dbuf_desc;
    return req;
put_dmabuf:
    dma_buf_put(dmabuf);
fail_out:
    ni_fence_free(req->ni_fence);
    ni_free_io_request(req);
    return ERR_PTR(ret);
}

static void ni_dma_work_func(struct work_struct *work)
{
    struct ni_dev *dev = container_of(work, struct ni_dev, dma_work);
    struct ni_ioreq *req;
    struct ni_dmabuf_desc *dbuf_desc;
    struct bio_vec *io_vec;
    unsigned long len;
    unsigned long offset;
    uint8_t *usr_mem, *dev_mem;
    int i;

    while (!list_empty(&dev->req_list)) {
        spin_lock(&dev->req_lock);
        if (list_empty(&dev->req_list)) {
            spin_unlock(&dev->req_lock);
            break;
        }

        req = list_first_entry(&dev->req_list, struct ni_ioreq, list);
        list_del(&req->list);
        spin_unlock(&dev->req_lock);

        dbuf_desc = req->dbuf_desc;
        BUG_ON(!dbuf_desc || !dbuf_desc->vaddr);

        dev_mem = dbuf_desc->vaddr;
        offset = 0;
        len = req->len;
        io_vec = req->io_vec;
        for (i = 0; i < req->vcnt; i++) {
            struct page *page = io_vec[i].bv_page;
            if (page) {
                usr_mem = kmap(page);
                if (req->dir == DMA_TO_DEVICE)
                    memcpy(dev_mem + offset, usr_mem + io_vec[i].bv_offset,
                            io_vec[i].bv_len);
                else
                    memcpy(usr_mem + io_vec[i].bv_offset, dev_mem + offset,
                            io_vec[i].bv_len);
                BUG_ON(len < io_vec[i].bv_len);
                len -= io_vec[i].bv_len;
                offset += io_vec[i].bv_len;
                kunmap(page);
            }
        }
        BUG_ON(len != 0);

        mutex_lock(&dev->req_timeline->mutex);
        dev->req_timeline->hwseqno++;
        ni_debug("%s: signal req fence: hwseqno %llu\n", __func__,
                dev->req_timeline->hwseqno);
        dma_fence_signal(&req->ni_fence->fence);
        mutex_unlock(&dev->req_timeline->mutex);

        ni_fence_put(req->ni_fence);
        ni_free_io_request(req);

        cond_resched();
    }
}

int ni_dev_init_ioreq(struct ni_dev *dev)
{
    int ret;

    snprintf(dev->name, NETINT_NI_DEV_NAME, "ni_dma_wq_%d", dev->devid);
    dev->bg_dma_wq = create_singlethread_workqueue(dev->name);
    if (!dev->bg_dma_wq) {
        ni_err("failed to create dma_wq for dev %d\n",dev->devid);
        return -ENOMEM;
    }

    dev->req_timeline = ni_timeline_alloc();
    if (!dev->req_timeline) {
        ni_err("failed to allocate timeline: dev %d\n", dev->devid);
        ret = -ENOMEM;
        goto destroy_dma_wq;
    }

    spin_lock_init(&dev->req_lock);
    INIT_LIST_HEAD(&dev->req_list);
    INIT_WORK(&dev->dma_work, ni_dma_work_func);

    return 0;

destroy_dma_wq:
    destroy_workqueue(dev->bg_dma_wq);
    return ret;
}

void ni_dev_free_ioreq(struct ni_dev *dev)
{
    struct ni_ioreq *req, *tmp;

    if (dev->bg_dma_wq) {
        flush_workqueue(dev->bg_dma_wq);
        destroy_workqueue(dev->bg_dma_wq);
        dev->bg_dma_wq = NULL;
    }

    if (!list_empty(&dev->req_list)) {
        ni_warn("ni_dev %d req_list not empty!!!\n", dev->devid);
        spin_lock(&dev->req_lock);
        list_for_each_entry_safe(req, tmp, &dev->req_list, list) {
            list_del(&req->list);
            ni_fence_put(req->ni_fence);
            ni_free_io_request(req);
        }
        spin_unlock(&dev->req_lock);
    }
    if (dev->req_timeline)
        ni_timeline_put(dev->req_timeline);
}

int ni_reschedule_ioreq(struct ni_dev *dev, struct ni_ioreq *req)
{
    spin_lock(&dev->req_lock);
    list_add_tail(&req->list, &dev->req_list);
    spin_unlock(&dev->req_lock);
    queue_work(dev->bg_dma_wq, &dev->dma_work);

    return 0;
}

int ni_ioreq_init(void)
{
    ni_ioreq_cache = kmem_cache_create("ni_ioreq", sizeof(struct ni_ioreq), 0,
            SLAB_HWCACHE_ALIGN, NULL);
    if (!ni_ioreq_cache) {
        ni_err("failed to create ioreq cache\n");
        return -ENOMEM;
    }

    atomic64_set(&ni_ioreq_count, 0);
    return 0;
}

void ni_ioreq_fini(void)
{
    kmem_cache_destroy(ni_ioreq_cache);
}
#endif
