#ifndef _NETINT_IMPORT_H
#define _NETINT_IMPORT_H

#include <linux/spinlock.h>
#include <linux/list.h>

extern struct ni_dmabuf_desc *ni_dmabuf_import(struct dma_buf *dmabuf);
extern void ni_dmabuf_import_free(struct ni_dmabuf_desc *dbus_desc);
#endif /* _NETINT_IMPORT_H */
